// 
// 
// 

#include "AccumulatorView.h"
#include "Display.h"

#include <UTFT/UTFT.h>

AccumulatorViewClass::AccumulatorViewClass(ViewClass * pParent/* = 0*/) : ViewClass(pParent)
{
	// construct the view
	Rect myRect(0, 0, 319, 239);
	SetFrame(myRect);

	SetBackgroundColor(VGA_BLACK);

	// add title
	m_pTitleLabel = new LabelClass(this);
	Rect r1(20, 25, 319 - 20 * 2, 30);
	m_pTitleLabel->SetFrame(r1);
	m_pTitleLabel->SetText("ACCUMULATOR");
	m_pTitleLabel->SetBackgroundColor(VGA_RED);
	m_pTitleLabel->SetTextColor(VGA_YELLOW);
	AddSubView(m_pTitleLabel);

	// add indicator
	m_pIndicator = new IndicatorClass(this);
	Rect r2(70, 70, 319 - 70 * 2, 70);
	m_pIndicator->SetFrame(r2);
	m_pIndicator->SetBorderWidth(1);
	m_pIndicator->SetBorderColor(VGA_GREEN);
	m_pIndicator->SetMinimum(10.0f);
	m_pIndicator->SetMaximum(15.0f);
	m_pIndicator->SetCurrent(14.5f);
	AddSubView(m_pIndicator);

	SetName("Accumulator");
	SetChanges(true);
}

AccumulatorViewClass::~AccumulatorViewClass()
{
}

void AccumulatorViewClass::SetCurrent(float f)
{
	m_pIndicator->SetCurrent(f);
	m_pIndicator->GetTitleLabel()->SetText(String(f) + " V");
}
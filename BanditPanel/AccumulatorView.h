// AccumulatorView.h

#ifndef _ACCUMULATORVIEW_h
#define _ACCUMULATORVIEW_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "View.h"
#include "Label.h"
#include "Indicator.h"

class AccumulatorViewClass : public ViewClass
{
 protected:
	LabelClass * m_pTitleLabel;
	IndicatorClass * m_pIndicator;

 private:

 public:
	AccumulatorViewClass(ViewClass * pParent = 0);
	virtual ~AccumulatorViewClass();

	void SetCurrent(float f);
};

#endif


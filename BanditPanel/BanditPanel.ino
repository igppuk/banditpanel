#include "EncoderListener.h"
#include "Display.h"
#include "Menu.h"
#include "EncoderWrapper.h"
#include "data.h"

#include <UTFT.h>
#include <DueTimer.h>
#include <Encoder.h>

#include "data.h"

//#include <URTouch.h>
//#include <UTFT_Buttons.h>

UTFT g_uftf(CTE32, 25, 26, 27, 28);
//URTouch g_touch(6, 5, 4, 3, 2);
//UTFT_Buttons g_buttons(&g_uftf, &g_touch);

DisplayClass g_display;
MenuClass g_menu;
EncoderWrapperClass g_encoder;

#define encoder0PinA  8
#define encoder0PinB  9
Encoder myEnc(8, 9);
ScreenData g_ScreenData;

void timerHandler();

void setup()
{
	Serial.begin(115200);

	g_uftf.InitLCD();
	g_uftf.clrScr();
	g_uftf.setFont(BigFont);

	//g_buttons.setTextFont(BigFont);
	//g_touch.InitTouch(1);
	//g_touch.setPrecision(PREC_MEDIUM);

	// show menu
	g_menu.init();
	g_display.init(&g_uftf, &g_menu);

	// initialize encoder
	g_encoder.init(&myEnc);
	g_encoder.AddListener((EncoderListenerClass *)&g_display);

	// initialize display store
	g_ScreenData.m_iDisplayWidth = g_uftf.getDisplayXSize();
	g_ScreenData.m_iDisplayHeight = g_uftf.getDisplayYSize();
	g_ScreenData.m_iXFontSize = g_uftf.getFontXsize();
	g_ScreenData.m_iYFontSize = g_uftf.getFontYsize();
	
	// initialize timer
	Timer6.attachInterrupt(timerHandler).setFrequency(10).start(); // 10 times per sec
}

void loop()
{
}

float f1 = 10.0f;
float f2 = 100.0f;
bool d1 = true;
bool d2 = true;
void timerHandler()
{
	g_encoder.tick();
	g_display.Update();
	g_display.foo1(f1);
	g_display.foo2(f2);

	if (d1)
	{
		if (f1 < 15.0f)
		{
			f1 += 0.15;
		}
		else
		{
			d1 = false;
		}
	}
	else if (!d1)
	{
		if (f1 > 10.0f)
		{
			f1 -= 0.15;
		}
		else
		{
			d1 = true;
		}
	}

	if (d2)
	{
		if (f2 < 12000.0f)
		{
			f2 += 500.0;
		}
		else
		{
			d2 = false;
		}
	}
	else if (!d2)
	{
		if (f2 > 100.0f)
		{
			f2 -= 500.0;
		}
		else
		{
			d2 = true;
		}
	}
}
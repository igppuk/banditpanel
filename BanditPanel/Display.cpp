// 
// 
// 
#include "Display.h"
#include "Menu.h"
#include "EncoderWrapper.h"
#include "AccumulatorView.h"
#include "TachometerView.h"

void DisplayClass::init(UTFT* pDisplay, MenuClass * pMenu)
{
	m_pDisplay = pDisplay;
	m_pMenu = pMenu;
	m_pMenu->Update(m_pDisplay);
}

void DisplayClass::MoveHandler(EncoderWrapperClass::Move eMove)
{
	if (eMove == EncoderWrapperClass::BACK)
	{
		m_pMenu->Back();
	}
	else if (eMove == EncoderWrapperClass::FORWARD)
	{
		m_pMenu->Forward();
	}

	m_pMenu->Update(m_pDisplay);
}

void DisplayClass::ShortClickHandler()
{
	m_pMenu->Click();

	m_pMenu->Reset(m_pDisplay);
	m_pMenu->Update(m_pDisplay);
}

void DisplayClass::LongClickHandler()
{
	m_pMenu->ClickBack();

	m_pMenu->Reset(m_pDisplay);
	m_pMenu->Update(m_pDisplay);
}

void DisplayClass::Update()
{
	ItemClass * pItem = m_pMenu->GetSelectedItem();
	if (pItem != NULL && pItem->GetActive())
	{
		if (pItem->HasView())
		{
			ViewClass * pView = pItem->GetView();
			if (pView)
			{
				// user clicked on menu item and should show the view
				pView->OnDraw(this);
			}
		}
	}
}

void DisplayClass::foo1(float f)
{
	ItemClass * pItem = m_pMenu->GetSelectedItem();
	if (pItem != NULL && pItem->GetActive())
	{
		if (pItem->HasView())
		{
			ViewClass * pView = pItem->GetView();
			if (pView->GetName().equals("Accumulator"))
			{
				((AccumulatorViewClass* )pView)->SetCurrent(f);
			}
		}
	}
}

void DisplayClass::foo2(float f)
{
	ItemClass * pItem = m_pMenu->GetSelectedItem();
	if (pItem != NULL && pItem->GetActive())
	{
		if (pItem->HasView())
		{
			ViewClass * pView = pItem->GetView();
			if (pView->GetName().equals("Tachometer"))
			{
				((TachometerViewClass* )pView)->SetValue(f);
			}
		}
	}
}

UTFT * DisplayClass::GetTFT()
{
	return m_pDisplay;
}
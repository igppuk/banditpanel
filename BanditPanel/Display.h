// Display.h

#ifndef _DISPLAY_h
#define _DISPLAY_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "EncoderListener.h"

class UTFT;
class MenuClass;
class EncoderWrapperClass;

class DisplayClass : EncoderListenerClass
{
 protected:
	UTFT * m_pDisplay;
	MenuClass * m_pMenu;

private:

 public:
	void init(UTFT* pDisplay, MenuClass * pMenu);
	void MoveHandler(EncoderWrapperClass::Move eMove);
	void ShortClickHandler();
	void LongClickHandler();
	void Update();
	void foo1(float f);
	void foo2(float f);

	UTFT * GetTFT();
};

#endif


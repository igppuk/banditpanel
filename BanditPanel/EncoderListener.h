// EncoderListener.h

#ifndef _ENCODERLISTENER_h
#define _ENCODERLISTENER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "EncoderWrapper.h"

class EncoderListenerClass
{
 public:
	virtual void MoveHandler(EncoderWrapperClass::Move eMove) {}
	virtual void ShortClickHandler() {}
	virtual void LongClickHandler() {}
};

#endif


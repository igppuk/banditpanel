// 
// 
// 

#include "EncoderWrapper.h"

#include <Encoder.h>
#include "EncoderListener.h"

#define ENCODER_BUTTON_PIN 11

void EncoderWrapperClass::init(Encoder * pEncoder)
{
	m_iCounter = 0;
	m_iOldCounter = 0;
	m_lLastMillis = 0;
	m_bClicking = false;
	m_bFirstClick = true;
	m_pEncoder = pEncoder;

	pinMode(ENCODER_BUTTON_PIN, OUTPUT);
	digitalWrite(ENCODER_BUTTON_PIN, LOW);

	pinMode(ENCODER_BUTTON_PIN, INPUT);
}

EncoderWrapperClass::Move EncoderWrapperClass::getMove()
{
	m_iCounter = m_pEncoder->read();

	if (m_iCounter > m_iOldCounter)
	{
		m_iOldCounter = m_iCounter;
		return Move::FORWARD;
	}
	else if (m_iCounter < m_iOldCounter)
	{
		m_iOldCounter = m_iCounter;
		return Move::BACK;
	}

	m_iOldCounter = m_iCounter;
	return Move::NOP;
}

void EncoderWrapperClass::tick()
{
	// check move
	EncoderWrapperClass::Move move = getMove();

	bool bShortClick = false;
	bool bLongClick = false;

	// check button
	int val = digitalRead(ENCODER_BUTTON_PIN);  // read input value
	if (val == HIGH && !m_bClicking)
	{
		if ((millis() - m_lLastMillis) >= 1500)
		{
			// this is a long click
			bLongClick = true;
			m_bClicking = true;
			bShortClick = false;
		}
	}
	else if (val == HIGH && m_bClicking)
	{
		m_lLastMillis = millis();
	}
	else if (val == LOW && !m_bClicking)
	{
		if ((millis() - m_lLastMillis) >= 250)
		{
			// this is a short click
			bShortClick = true;
		}
		m_lLastMillis = millis();
	}
	else if (val == LOW)
	{
		m_bClicking = false;
	}

	if (m_bFirstClick && bShortClick)
	{
		m_bFirstClick = false;
		bShortClick = false;
		bLongClick = false;
	}

	vector<EncoderListenerClass *>::iterator it = m_aListeners.begin();
	for (; it != m_aListeners.end(); it++)
	{
		if (move == FORWARD || move == BACK)
		{
			(*it)->MoveHandler(move);
		}
		if (bShortClick)
		{
			(*it)->ShortClickHandler();
		}
		if (bLongClick)
		{
			(*it)->LongClickHandler();
		}
	}
}

void EncoderWrapperClass::AddListener(EncoderListenerClass * pListener)
{
	m_aListeners.push_back(pListener);
}
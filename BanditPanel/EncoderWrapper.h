// EncoderWrapper.h

#ifndef _ENCODER_WRAPPER_h
#define _ENCODER_WRAPPER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include <vector>

using namespace std;

class Encoder;
class EncoderListenerClass;

class EncoderWrapperClass
{
 protected:

 private:
	vector<EncoderListenerClass *> m_aListeners;
	Encoder * m_pEncoder;
	int m_iCounter;
	int m_iOldCounter;
	unsigned long m_lLastMillis;
	bool m_bClicking;
	bool m_bFirstClick;

 public:
	enum Move
	{
		FORWARD,
		BACK,
		NOP
	};

	void init(Encoder * pEncoder);
	void tick();

	void AddListener(EncoderListenerClass * pListener);

 private:
	Move getMove();
};

#endif


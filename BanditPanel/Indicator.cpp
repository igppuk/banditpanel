// 
// 
// 

#include "Indicator.h"
#include "Display.h"

#include <UTFT/UTFT.h>

IndicatorClass::IndicatorClass(ViewClass * pParent) : ViewClass(pParent), m_fMin(0.0f), m_fMax(0.0f), m_fCurrent(0.0f)
{
	// add title
	m_pTitleLabel = new LabelClass(this);
	m_pTitleLabel->SetText("Charge");
	m_pTitleLabel->SetTextColor(VGA_YELLOW);
	
	// add outer rect
	m_pIndicatorView = new ViewClass(this);
	m_pIndicatorView->SetBorderWidth(1);
	m_pIndicatorView->SetBorderColor(VGA_WHITE);

	AddSubView(m_pTitleLabel);
	AddSubView(m_pIndicatorView);

	SetName("Indicator");
	SetChanges();
}

IndicatorClass::~IndicatorClass()
{
}

void IndicatorClass::SetMinimum(float fMin)
{
	m_fMin = fMin;
	SetChanges();
}

void IndicatorClass::SetMaximum(float fMax)
{
	m_fMax = fMax;
	SetChanges();
}

void IndicatorClass::SetCurrent(float fCurrent)
{
	m_fCurrent = fCurrent;
	SetChanges();
}

const float IndicatorClass::GetCurrent()
{
	return m_fCurrent;
}

LabelClass * const IndicatorClass::GetTitleLabel()
{
	return m_pTitleLabel;
}

ViewClass * const IndicatorClass::GetIndicatorView()
{
	return m_pIndicatorView;
}

void IndicatorClass::OnDraw(DisplayClass * pDisplay)
{
	Rect rect = GetFrame();
	if (rect.m_dy < 45)
		return;

	if (!IsChanged())
		return;

	if (!pDisplay)
		return;

	UTFT * pT = pDisplay->GetTFT();
	if (!pT)
		return;

	// autoresize
	Rect titleRect = ((ViewClass *)GetTitleLabel())->GetFrame();
	Rect indicatorRect = ((ViewClass *)GetIndicatorView())->GetFrame();

	Rect newTitleRect = Rect(2, 0, rect.m_dx - 2 * 2, 30);
	if (titleRect != newTitleRect)
		((ViewClass *)GetTitleLabel())->SetFrame(newTitleRect);

	Rect newIndicatorRect = Rect(2, 35, rect.m_dx - 2 * 2, rect.m_dy - 37);
	if (indicatorRect != newIndicatorRect)
		((ViewClass *)GetIndicatorView())->SetFrame(newIndicatorRect);

	ViewClass::OnDraw(pDisplay);

	// draw the indicator
	// 0-60 percents is yellow
	// 60-90 percents is green
	// 90-100 percents is red

	float fDiff = m_fMax - m_fMin;
	if (fDiff <= 0)
		return;

	GetIndicatorView()->GetRealFrame(indicatorRect);
	int x0 = 0;
	int x60 = 0.6 * indicatorRect.m_dx;
	int x90 = 0.9 * indicatorRect.m_dx;
	int x100 = indicatorRect.m_dx;
	int xCurrent = (m_fCurrent - m_fMin) / (m_fMax - m_fMin) * indicatorRect.m_dx;

	for (int i = x0; i < x100; i++)
	{
		if (i >= xCurrent)
		{
			pT->setColor(GetBackgroundColor());
		}
		else if (i < x60)
		{
			pT->setColor(VGA_YELLOW);
		}
		else if (i < x90)
		{
			pT->setColor(VGA_GREEN);
		}
		else
		{
			pT->setColor(VGA_RED);
		}

		pT->drawVLine(indicatorRect.m_x + i, indicatorRect.m_y, indicatorRect.m_dy);
	}
}
// Indicator.h

#ifndef _INDICATOR_h
#define _INDICATOR_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "View.h"
#include "Label.h"

class DisplayClass;

class IndicatorClass : public ViewClass
{
 protected:
	float m_fMin;
	float m_fMax;
	float m_fCurrent;
	LabelClass * m_pTitleLabel;
	ViewClass * m_pIndicatorView;;

 public:
	IndicatorClass(ViewClass * pParent);
	virtual ~IndicatorClass();

	void SetMinimum(float fMin);
	void SetMaximum(float fMax);
	void SetCurrent(float fCurrent);
	const float GetCurrent();
	LabelClass * const GetTitleLabel();
	ViewClass * const GetIndicatorView();

	virtual void OnDraw(DisplayClass * pDisplay);
};

#endif


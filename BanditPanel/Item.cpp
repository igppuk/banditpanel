// 
// 
// 

#include "Item.h"
#include "AccumulatorView.h"
#include "TachometerView.h"

ItemClass::~ItemClass()
{
	delete m_pView;

	vector<ItemClass *>::iterator it = m_aChilds.begin();
	for (; it != m_aChilds.end(); it++)
	{
		delete (*it);
	}
}

ItemClass::ItemClass(const String & sName, const String & sViewName, const unsigned int iId, ItemClass * pParent) : m_height(25), m_width(295), m_pView(0)
{
	m_bActive = false;
	m_sName = sName.c_str();
	m_sViewName = sViewName.c_str();
	m_iId = iId;
	m_pParent = pParent;
}

void ItemClass::SetView(ViewClass * pView)
{
	m_pView = pView;
}

bool ItemClass::HasView()
{
	return (m_sViewName.length() > 0);
}

ViewClass * const ItemClass::GetView()
{
	if (GetActive() && !m_pView)
	{
		if (m_sViewName.equals("AccumulatoView"))
		{
			m_pView = new AccumulatorViewClass();
		}
		else if (m_sViewName.equals("TachometerView"))
		{
			m_pView = new TachometerViewClass();
		}
	}

	return m_pView;
}

ItemClass * ItemClass::AddChild(const String& sName, const String & sViewName, const unsigned int iId)
{
	ItemClass * pChild = new ItemClass(sName, sViewName, iId, this);

	m_aChilds.push_back(pChild);
	return pChild;
}

ItemClass * ItemClass::GetParent()
{
	return m_pParent;
}

const std::vector<ItemClass *>& ItemClass::GetChilds()
{
	return m_aChilds;
}

const String & ItemClass::GetName()
{
	return m_sName;
}

const unsigned int ItemClass::GetId()
{
	return m_iId;
}

const unsigned int ItemClass::GetHeight()
{
	return m_height;
}

const unsigned int ItemClass::GetWidth()
{
	return m_width;
}

void ItemClass::SetActive(bool bActive/* = true*/)
{
	m_bActive = bActive;

	if (!bActive && m_pView)
	{
		delete m_pView;
		m_pView = 0;
	}
}

bool ItemClass::GetActive()
{
	return m_bActive;
}
// Item.h

#ifndef _ITEM_h
#define _ITEM_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include <vector>
#include "View.h"

using namespace std;

class ItemClass
{

public:
	ItemClass * AddChild(const String & sName, const String & sViewName, const unsigned int iId);
	const vector<ItemClass *>& GetChilds();
	const String & GetName();
	const unsigned int GetId();
	const unsigned int GetHeight();
	const unsigned int GetWidth();
	ItemClass * GetParent();
protected:
	String m_sName;
	String m_sViewName;
	unsigned int m_iId;
	unsigned int m_height;
	unsigned int m_width;

private:
	vector<ItemClass *> m_aChilds;
	ItemClass * m_pParent;
	ViewClass * m_pView;
	bool m_bActive;

 public:
	~ItemClass();
	ItemClass(const String & sName, const String & sViewName, const unsigned int iId, ItemClass * pParent = 0);

	void SetView(ViewClass * pView);
	bool HasView();
	ViewClass * const GetView();
	void SetActive(bool bActive = true);
	bool GetActive();
};

#endif


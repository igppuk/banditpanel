// 
// 
// 

#include "Label.h"
#include "Display.h"

#include <UTFT.h>
#include "data.h"

LabelClass::LabelClass(ViewClass * pParent/* = 0*/) : ViewClass(pParent)
{
	m_sText = String();
	SetAlign(Center);
	SetVerticalAlign(Middle);
	SetTextColor(VGA_WHITE);
	SetBackgroundColor(VGA_TRANSPARENT);
	SetFontSize(Medium);
	SetName("Label");
	SetChanges();
}

LabelClass::~LabelClass()
{
}

void LabelClass::SetText(const String & sText, int iDeg)
{
	m_sText = sText;
	m_iDeg = iDeg;
	SetChanges();
}

const String & LabelClass::GetText()
{
	return m_sText;
}

void LabelClass::SetTextColor(word wColor)
{
	m_wTextColor = wColor;
	SetChanges();
}

const word LabelClass::GetTextColor()
{
	return m_wTextColor;
}

void LabelClass::SetAlign(Align eAlign)
{
	m_eAlign = eAlign;
}

void LabelClass::SetVerticalAlign(VAlign eVAlign)
{
	m_eAVlign = eVAlign;
}

void LabelClass::SetFontSize(FontSize eFontSize)
{
	m_eFontSize = eFontSize;
}

void LabelClass::OnDraw(DisplayClass * pDisplay)
{
	if (!IsChanged())
		return;

	if (!pDisplay)
		return;

	UTFT * pT = pDisplay->GetTFT();
	if (!pT)
		return;

	ViewClass::OnDraw(pDisplay);

	// save ths old font
	uint8_t * pOldFont = pT->getFont();

	if (m_eFontSize == Small)
	{
		pT->setFont(SmallFont);
	}
	else if (m_eFontSize == Medium)
	{
		pT->setFont(BigFont);
	}
	else if (m_eFontSize == Big)
	{
		pT->setFont(SevenSegNumFont);
	}

	g_ScreenData.m_iXFontSize = pT->getFontXsize();
	g_ScreenData.m_iYFontSize = pT->getFontYsize();

	int x = m_frame.m_x;
	int y = m_frame.m_y;

	// align horizontal
	int iTextWidth = GetText().length() * g_ScreenData.m_iXFontSize;
	if (m_eAlign == Center)
	{
		if (iTextWidth < m_frame.m_dx)
		{
			x += (m_frame.m_dx - iTextWidth) / 2;
		}
	}
	else if (m_eAlign == Rigth)
	{
		if (iTextWidth < m_frame.m_dx)
		{
			x += m_frame.m_dx - iTextWidth;
		}
	}

	// align vertical
	int iTextHeight = g_ScreenData.m_iYFontSize;
	if (m_eAVlign == Middle)
	{
		if (iTextHeight < m_frame.m_dy)
		{
			y += (m_frame.m_dy - iTextHeight) / 2;
		}
	}
	else if (m_eAVlign == Bottom)
	{
		if (iTextHeight < m_frame.m_dy)
		{
			y += m_frame.m_dy - iTextHeight;
		}
	}

	if (m_pParent)
	{
		const Rect & parentRect = m_pParent->GetFrame();
		x += parentRect.m_x;
		y += parentRect.m_y;
	}

	// clear old text
	Rect realFrame;
	GetRealFrame(realFrame);

	pT->setColor(GetBackgroundColor());
	pT->fillRect(realFrame.m_x, realFrame.m_y, realFrame.m_x + realFrame.m_dx, realFrame.m_y + realFrame.m_dy);

	pT->setColor(m_wTextColor);
	pT->setBackColor(GetBackgroundColor());
	pT->print(m_sText, x, y, m_iDeg);

	// restore the old font
	pT->setFont(pOldFont);
	g_ScreenData.m_iXFontSize = pT->getFontXsize();
	g_ScreenData.m_iYFontSize = pT->getFontYsize();
}
// Label.h

#ifndef _LABEL_h
#define _LABEL_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "View.h"

class DisplayClass;

typedef enum Align
{
	Left,
	Center,
	Rigth
};

typedef enum VAlign
{
	Top,
	Middle,
	Bottom
};

typedef enum FontSize
{
	Small,
	Medium,
	Big
};

class LabelClass : public ViewClass
{
 protected:
	String m_sText;
	int m_iDeg;
	word m_wTextColor;
	Align m_eAlign;
	VAlign m_eAVlign;
	FontSize m_eFontSize;

 public:
	LabelClass(ViewClass * pParent = 0);
	virtual ~LabelClass();

	void SetText(const String & sText, int iDeg = 0);
	const String & GetText();
	void SetTextColor(word wColor);
	const word GetTextColor();
	void SetAlign(Align eAlign);
	void SetVerticalAlign(VAlign eVAlign);
	void SetFontSize(FontSize eFontSize);

	virtual void OnDraw(DisplayClass * pDisplay);
};

#endif


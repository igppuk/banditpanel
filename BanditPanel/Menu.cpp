// 
// 
// 

#include "MenuTemplate.h"
#include "Menu.h"

void MenuClass::init()
{
	m_marginLeft = 20;
	m_marginTop = 20;
	m_itemMargin = 20;

	// initialize menu
	int menuSize = sizeof(menuObj) / sizeof(MenuEntry);

	// initialize all roots
	for (int i = 0; i < menuSize; i++)
	{
		MenuEntry entry = menuObj[i];
		if (entry.ParentId == 0)
		{
			ItemClass * pRoot = new ItemClass(entry.Name, entry.ViewName, entry.Id);
			m_aRoots.push_back(pRoot);
		}
	}

	// initialize all childs
	for (int i = 0; i < menuSize; i++)
	{
		MenuEntry entry = menuObj[i];
		if (entry.ParentId > 0)
		{
			ItemClass * pParent = GetMenuItemById(entry.ParentId);
			if (pParent)
			{
				pParent->AddChild(entry.Name, entry.ViewName, entry.Id);
			}
		}
	}

	m_pCurrentRoots = 0;
	if (m_aRoots.size() > 0)
	{
		m_spActiveItem = m_aRoots[0];
	}

	/*ItemClass * root1 = new ItemClass("Speed / Tachometer", 1);
	ItemClass * root2 = new ItemClass("Battery", 2);
	ItemClass * root3 = new ItemClass("Engine", 3);
	ItemClass * root4 = new ItemClass("Settings", 4);

	ItemClass * t1 = new ItemClass("temp1", 20);
	ItemClass * t2 = new ItemClass("temp2", 21);
	ItemClass * t3 = new ItemClass("temp3", 22);
	ItemClass * t4 = new ItemClass("temp4", 23);
	ItemClass * t5 = new ItemClass("temp5", 24);
	ItemClass * t6 = new ItemClass("temp6", 25);
	ItemClass * t7 = new ItemClass("temp7", 26);
	ItemClass * t8 = new ItemClass("temp8", 27);
	ItemClass * t9 = new ItemClass("temp9", 29);
	ItemClass * t10 = new ItemClass("temp10", 30);

	m_aRoots.push_back(root1);
	m_aRoots.push_back(root2);
	m_aRoots.push_back(root3);
	m_aRoots.push_back(root4);

	m_aRoots.push_back(t1);
	m_aRoots.push_back(t2);
	m_aRoots.push_back(t3);
	m_aRoots.push_back(t4);
	m_aRoots.push_back(t5);
	m_aRoots.push_back(t6);
	m_aRoots.push_back(t7);
	m_aRoots.push_back(t8);
	m_aRoots.push_back(t9);
	m_aRoots.push_back(t10);*/

	m_pCurrentRoots = &m_aRoots;

	/*m_spActiveItem = root4;

	// battery
	root2->AddChild("Diagnostics", 5);
	root2->AddChild("Settings", 6);

	// engine
	root3->AddChild("Diagnostics", 7);
	root3->AddChild("Settings", 8);

	// settings
	ItemClass * t100 = root4->AddChild("Arduino", 9);
	root4->AddChild("Bike", 10);
	root4->AddChild("Network", 11);

	t100->AddChild("aaa", 101);*/

	Reset();
}

void MenuClass::destroy()
{
	vector<ItemClass *>::iterator it = m_aRoots.begin();
	for (; it != m_aRoots.end(); it++)
	{
		delete (*it);
	}
}

ItemClass * MenuClass::GetSelectedItem()
{
	return m_spActiveItem;
}

void MenuClass::Reset(UTFT * pDisplay)
{
	if (m_bLock)
		return;

	if (pDisplay)
		pDisplay->clrScr();

	if (m_pCurrentRoots == 0)
		return;

	m_beginIt = m_pCurrentRoots->begin();
	m_finishIt = m_pCurrentRoots->begin();

	const unsigned int visibleItems = 5;
	vector<ItemClass *>::iterator it_begin = m_pCurrentRoots->begin();
	for (int i = 0; it_begin != m_pCurrentRoots->end(); it_begin++, i++)
	{
		if (i >= (visibleItems - 1))
		{
			return;
		}
		m_finishIt++;
	}

	m_finishIt = m_pCurrentRoots->end();
	m_finishIt--;
}

void MenuClass::Update(UTFT * pDisplay)
{
	if (m_bLock)
		return;

	if (m_pCurrentRoots == 0)
		return;

	m_displayWidth = pDisplay->getDisplayXSize();
	m_displayHeight = pDisplay->getDisplayYSize();

	int y = m_marginTop;

	const unsigned int visibleItems = 5;
	vector<ItemClass *>::iterator it_begin = m_beginIt;
	vector<ItemClass *>::iterator it_finish = m_finishIt;

	bool inside = false;
	for (; it_begin != m_pCurrentRoots->end(); it_begin++)
	{
		if ((*it_begin)->GetId() == m_spActiveItem->GetId())
		{
			inside = true;
			break;
		}

		if ((*it_begin)->GetId() == (*it_finish)->GetId())
		{
			break;
		}
	}

	if (!inside)
	{
		it_begin = m_pCurrentRoots->begin();
		bool iMove = false;
		int iCounter = visibleItems;
		for (; it_begin != m_pCurrentRoots->end(); it_begin++)
		{
			if (iMove)
			{
				if (--iCounter <= 0)
				{
					break;
				}
			}
			if ((*it_begin)->GetId() == m_spActiveItem->GetId())
			{
				iMove = true;
				m_beginIt = it_begin;
			}

			m_finishIt = it_begin;
		}

		pDisplay->clrScr();
	}

	// draw items
	it_begin = m_beginIt;
	it_finish = m_finishIt;
	for (int i = 0; it_begin != m_pCurrentRoots->end(); it_begin++, i++)
	{
		ItemClass * itemClass = (*it_begin);
		String itemName = itemClass->GetName();

		pDisplay->setBackColor(VGA_TRANSPARENT);
		pDisplay->setColor(VGA_WHITE);
		pDisplay->print(itemName, m_marginLeft, y);

		int rectX = m_marginLeft - 2;
		int rectY = y - m_marginTop / 2;
		int rectWidth = rectX + itemClass->GetWidth();
		int rectHeight = rectY + itemClass->GetHeight() + m_itemMargin / 2;
		word currentColor = pDisplay->getColor();

		// check active item and draw the rect
		if (itemClass->GetId() == m_spActiveItem->GetId())
		{
			// draw border
			pDisplay->setColor(255, 127, 0);
			pDisplay->drawRect(rectX, rectY, rectWidth, rectHeight);
		}
		else
		{
			// remove border
			pDisplay->setColor(0, 0, 0);
			pDisplay->drawRect(rectX, rectY, rectWidth, rectHeight);
		}
		pDisplay->setColor(currentColor);

		y += itemClass->GetHeight() + m_itemMargin;

		if ((*it_begin)->GetId() == (*it_finish)->GetId())
		{
			break;
		}
	}
}

void MenuClass::Forward()
{
	if (m_bLock)
		return;

	vector<ItemClass *>::iterator it = m_pCurrentRoots->begin();
	for (; it != m_pCurrentRoots->end(); it++)
	{
		if ((*it)->GetId() == m_spActiveItem->GetId())
		{
			it++;
			if (it != m_pCurrentRoots->end())
			{
				m_spActiveItem = (*it);
			}
			break;
		}
	}
}

void MenuClass::Back()
{
	if (m_bLock)
		return;

	vector<ItemClass *>::iterator it = m_pCurrentRoots->begin();
	for (; it != m_pCurrentRoots->end(); it++)
	{
		if ((*it)->GetId() == m_spActiveItem->GetId())
		{
			if (it != m_pCurrentRoots->begin())
			{
				it--;
				m_spActiveItem = (*it);
			}
			break;
		}
	}
}

void MenuClass::Click()
{
	if (m_bLock)
		return;

	SetLock(false);
	const vector<ItemClass *> & childs = m_spActiveItem->GetChilds();
	if (childs.size() > 0)
	{
		ItemClass * firstChild = childs[0];
		m_spActiveItem = firstChild;
		m_spActiveItem->SetActive(false);
		SetLock(false);

		m_pCurrentRoots = &(vector<ItemClass *> &)childs;
	}
	else
	{
		if (m_spActiveItem->HasView())
		{
			SetLock(true);
			m_spActiveItem->SetActive(true);
		}
	}
}

void MenuClass::ClickBack()
{
	SetLock(false);

	if (m_spActiveItem->HasView() && m_spActiveItem->GetActive())
	{
		m_spActiveItem->SetActive(false);
		return;
	}

	// find parent of active item
	m_spActiveItem->SetActive(false);
	ItemClass * pParent = m_spActiveItem->GetParent();
	if (pParent)
	{
		m_spActiveItem = pParent;

		pParent = m_spActiveItem->GetParent();
		pParent->SetActive(false);
		if (!pParent)
		{
			m_pCurrentRoots = &m_aRoots;
		}
		else
		{
			const vector<ItemClass *> & childs = pParent->GetChilds();
			m_pCurrentRoots = &(vector<ItemClass *> &)childs;
		}
	}
	else
	{
		m_pCurrentRoots = &m_aRoots;

		if (m_pCurrentRoots->size() > 0)
		{
			ItemClass * firstChild = m_pCurrentRoots->at(0);
			m_spActiveItem = firstChild;
			m_spActiveItem->SetActive(false);
		}
	}
}

ItemClass * MenuClass::GetMenuItemById(int Id)
{
	for (int i = 0; i < m_aRoots.size(); i++)
	{
		ItemClass * item = m_aRoots[i];
		ItemClass * pFind = GetMenuItemRecursive(item, Id);
		if (pFind)
		{
			return pFind;
		}
	}

	return 0;
}

ItemClass * MenuClass::GetMenuItemRecursive(ItemClass * pParent, int Id)
{
	if (pParent->GetId() == Id)
	{
		return pParent;
	}

	const vector<ItemClass *>& pChilds = pParent->GetChilds();
	for (int i = 0; i < pChilds.size(); i++)
	{
		ItemClass * pFind = GetMenuItemRecursive(pChilds[i], Id);
		if (pFind)
		{
			return pFind;
		}
	}

	return 0;
}

void MenuClass::SetLock(bool bLock/* = true*/)
{
	m_bLock = bLock;
}
// Menu.h

#ifndef _MENU_h
#define _MENU_h

#include <vector>
#include "Item.h"

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include <UTFT/UTFT.h>

using namespace std;

class MenuClass
{
 public:
	ItemClass * GetSelectedItem();
	void Update(UTFT * pDisplay);
	void Reset(UTFT * pDisplay = 0);
	void Forward();
	void Back();
	void Click();
	void ClickBack();

 protected:
 private:
	vector<ItemClass *> m_aRoots;
	vector<ItemClass *> * m_pCurrentRoots;
	unsigned int m_displayWidth;
	unsigned int m_displayHeight;
	ItemClass * m_spActiveItem;
	unsigned int m_marginLeft;
	unsigned int m_marginTop;
	unsigned int m_itemMargin;
	bool m_bLock;

	vector<ItemClass *>::iterator m_beginIt;
	vector<ItemClass *>::iterator m_finishIt;

	ItemClass * GetMenuItemById(int Id);
	ItemClass * GetMenuItemRecursive(ItemClass * pParent, int Id);

 public:
	void init();
	void destroy();
	void SetLock(bool bLock = true);
};

#endif


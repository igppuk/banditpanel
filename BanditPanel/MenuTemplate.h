// MenuTemplate.h

#ifndef _MENUTEMPLATE_h
#define _MENUTEMPLATE_h

typedef struct {
	const char * Name;
	unsigned int Id;
	unsigned int ParentId;
	const char * ViewName;
} MenuEntry;


MenuEntry menuObj[] = {
	// root
	{"item1", 1, 0, ""},
	{"item2", 2, 0, ""},
	{"item3", 3, 0, ""},
	{"item4", 4, 0, ""},
	{"item5", 5, 0, ""},
	{"item6", 6, 0, ""},
	{"item7", 7, 0, ""},
	{"item8", 8, 0, ""},
	{"item9", 9, 0, ""},

	// item1 child
	{"item1_1", 10, 1, ""},
	{"item1_2", 11, 1, ""},
	{"item1_3", 12, 1, ""},
	{"item1_4", 13, 1, ""},

	// item1_1 child
	{"item1_1_1", 110, 10, "AccumulatoView"},
	{"item1_1_2", 111, 10, "TachometerView"},

	// item2 child
	{"item2_1", 20, 2, ""},
	{"item2_2", 21, 2, ""},
	{"item2_2", 22, 2, ""}
};

#endif


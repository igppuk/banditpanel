//
//
//

#include "TachometerView.h"
#include "Display.h"

#include <UTFT.h>
#include "data.h"

TachometerViewClass::TachometerViewClass(ViewClass * pParent/* = 0*/) : ViewClass(pParent), m_iValue(0), m_iRange(12000), m_iOldX(0)
{
	Rect myRect(0, 0, 319, 239);
	SetFrame(myRect);

	SetBackgroundColor(VGA_BLACK);

	// add title
	m_pTitleLabel = new LabelClass(this);
	Rect r1(20, 25, 319 - 20 * 2, 30);
	m_pTitleLabel->SetFrame(r1);
	m_pTitleLabel->SetText("TACHOMETER");
	m_pTitleLabel->SetBackgroundColor(VGA_RED);
	m_pTitleLabel->SetTextColor(VGA_YELLOW);
	AddSubView(m_pTitleLabel);

	// add counter
	m_pCounterLabel = new LabelClass(this);
	Rect r2(20, 70, 319 - 20 * 2, 60);
	m_pCounterLabel->SetFrame(r2);
	m_pCounterLabel->SetText("00000");
	m_pCounterLabel->SetTextColor(VGA_YELLOW);
	m_pCounterLabel->SetFontSize(Big);
	AddSubView(m_pCounterLabel);

	SetName("Tachometer");
	SetChanges();
}

TachometerViewClass::~TachometerViewClass()
{
}

void TachometerViewClass::SetRange(const int iRange)
{
	m_iRange = iRange;
	SetChanges();
}

void TachometerViewClass::SetValue(const int iValue)
{
	m_iValue = iValue;
	m_pCounterLabel->SetText(String(m_iValue));
	SetChanges();
}

void TachometerViewClass::OnDraw(DisplayClass * pDisplay)
{
	if (!IsChanged())
		return;

	if (!pDisplay)
		return;

	UTFT * pT = pDisplay->GetTFT();
	if (!pT)
		return;

	ViewClass::OnDraw(pDisplay);

	// calculate angel
	float fValue = (float)m_iValue / (float)m_iRange;

	// draw green left line
	pT->setColor(VGA_GREEN);
	
	int dx = 20;
	int x1 = dx;
	int y1 = 140;
	int x2 = g_ScreenData.m_iDisplayWidth / 2;
	int y2 = g_ScreenData.m_iDisplayHeight - 2;
	pT->drawLine(x1, y1, x2, y2);

	// draw red right line
	pT->setColor(VGA_RED);
	x1 = g_ScreenData.m_iDisplayWidth - dx;
	y1 = 140;
	x2 = g_ScreenData.m_iDisplayWidth / 2;
	y2 = g_ScreenData.m_iDisplayHeight - 2;
	pT->drawLine(x1, y1, x2, y2);

	pT->setColor(VGA_WHITE);
	x1 = dx + (g_ScreenData.m_iDisplayWidth - dx * 2) * fValue;
	y1 = 140;
	x2 = g_ScreenData.m_iDisplayWidth / 2;
	y2 = g_ScreenData.m_iDisplayHeight - 2;

	if (m_iOldX == 0)
	{
		m_iOldX = x1;
	}

	if (m_iOldX != x1)
	{
		pT->setColor(GetBackgroundColor());
		pT->drawLine(m_iOldX, y1, x2, y2);
		pT->drawLine(m_iOldX + 1, y1, x2, y2);
		pT->drawLine(m_iOldX + 2, y1, x2, y2);
		pT->drawLine(m_iOldX + 3, y1, x2, y2);
		pT->drawLine(m_iOldX + 4, y1, x2, y2);
		pT->drawLine(m_iOldX + 5, y1, x2, y2);
		m_iOldX = x1;
	}

	pT->setColor(VGA_WHITE);
	pT->drawLine(x1, y1, x2, y2);
	pT->drawLine(x1 + 1, y1, x2, y2);
	pT->drawLine(x1 + 2, y1, x2, y2);
	pT->drawLine(x1 + 3, y1, x2, y2);
	pT->drawLine(x1 + 4, y1, x2, y2);
	pT->drawLine(x1 + 5, y1, x2, y2);

	/*for (int x = dx + 2; x < dx + (g_ScreenData.m_iDisplayWidth - dx * 2) - 2; x++)
	{
		if (x < x1)
		{
			pT->setColor(VGA_WHITE);
		}
		else
		{
			pT->setColor(GetBackgroundColor());
		}

		pT->drawLine(x, y1, x2, y2);
	}*/

	SetChanges(false);
}

// TachometerView.h

#ifndef _TACHOMETERVIEW_h
#define _TACHOMETERVIEW_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "View.h"
#include "Label.h"

class TachometerViewClass : public ViewClass
{
 private:
	int m_iValue;
	int m_iRange;
	int m_iOldX;

 protected:
	LabelClass * m_pTitleLabel;
	LabelClass * m_pCounterLabel;

 public:
	TachometerViewClass(ViewClass * pParent = 0);
	virtual ~TachometerViewClass();

	void SetRange(const int iRange);
	void SetValue(const int iValue);

	virtual void OnDraw(DisplayClass * pDisplay);
};

#endif


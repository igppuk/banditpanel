// 
// 
// 

#include "View.h"
#include "UTFT.h"
#include "Display.h"
#include "Label.h"
#include "Indicator.h"
#include "AccumulatorView.h"

ViewClass::ViewClass(ViewClass * pParent/* = 0*/)
{
	m_pParent = pParent;
	m_frame.m_x = 0;
	m_frame.m_y = 0;
	m_frame.m_dx = 0;
	m_frame.m_dy = 0;
	m_oldFrame = m_frame;
	m_wBackgroundColor = VGA_TRANSPARENT;
	m_wBorderColor  = VGA_YELLOW;
	m_iBorderWidth = 0;
	m_bHasFocus = false;
	m_sName = "View";
	SetChanges();
}

ViewClass::~ViewClass()
{
	vector<ViewClass *>::iterator it = m_aSubViews.begin();
	for (; it != m_aSubViews.end(); it++)
	{
		delete (*it);
	}

	m_aSubViews.clear();
}

bool ViewClass::IsChanged()
{
	return m_bHasChanges;
}

void ViewClass::SetName(const String & sText)
{
	m_sName = sText;
}

const String & ViewClass::GetName()
{
	return m_sName;
}

const Rect & ViewClass::GetFrame()
{
	return m_frame;
}

void ViewClass::GetRealFrameRecursive(ViewClass *pParent, Rect* pRect)
{
	if (!pParent)
		return;

	const Rect & parentRect = pParent->GetFrame();
	pRect->m_x += parentRect.m_x;
	pRect->m_y += parentRect.m_y;
	
	GetRealFrameRecursive(pParent->GetParent(), pRect);
}

void ViewClass::GetRealFrame(Rect & frame)
{
	frame = GetFrame();
	GetRealFrameRecursive(GetParent(), &frame);
}

void ViewClass::SetFrame(const Rect & frame)
{
	m_frame = frame;
	SetChanges();
}

void ViewClass::AddSubView(ViewClass * pView)
{
	if (!pView)
		return;

	SetChanges();
	m_aSubViews.push_back(pView);
}

const vector<ViewClass *> * ViewClass::GetSubViews()
{
	return &m_aSubViews;
}

void ViewClass::RemoveView(ViewClass * pView)
{
	vector<ViewClass *>::iterator it = m_aSubViews.begin();
	for (; it != m_aSubViews.end(); it++)
	{
		if ((*it) == pView)
		{
			m_aSubViews.erase(it);
			break;
		}
	}

	SetChanges();
}

void ViewClass::RemoveFromSuperview()
{
	m_pParent->RemoveView(this);
}

const word ViewClass::GetBackgroundColor()
{
	if (m_wBackgroundColor == VGA_TRANSPARENT)
	{
		ViewClass * pParent = GetParent();
		if (pParent)
			return pParent->GetBackgroundColor();

		return VGA_BLACK;
	}

	return m_wBackgroundColor;
}

void ViewClass::SetBackgroundColor(word wBackgroundColor)
{
	m_wBackgroundColor = wBackgroundColor;
	SetChanges();
}

const word ViewClass::GetBorderColor()
{
	return m_wBorderColor;
}

void ViewClass::SetBorderColor(word wBorderColor)
{
	m_wBorderColor = wBorderColor;
	SetChanges();
}

const unsigned int ViewClass::GetBorderWidth()
{
	return m_iBorderWidth;
}

void ViewClass::SetBorderWidth(unsigned int iBorderWidth)
{
	m_iBorderWidth = iBorderWidth;
	SetChanges();
}

void ViewClass::SetFocus()
{
	m_bHasFocus = true;
	SetChanges();
}

void ViewClass::ReleaseFocus()
{
	m_bHasFocus = false;
	SetChanges();
}

ViewClass * const ViewClass::GetParent()
{
	return m_pParent;
}

void ViewClass::SetChanges(bool bChanges/* = true*/)
{
	SetChangesRecursive(this, bChanges);
}

void ViewClass::SetChangesRecursive(ViewClass * pView, bool bChanges)
{
	pView->m_bHasChanges = bChanges;
	ViewClass * pParent = pView->GetParent();
	if (!pParent)
		return;

	SetChangesRecursive(pParent, bChanges);
}

void ViewClass::OnDraw(DisplayClass * pDisplay)
{
	if (!IsChanged())
		return;

	if (!pDisplay)
		return;

	UTFT * pT = pDisplay->GetTFT();
	if (!pT)
		return;

	int x = m_frame.m_x;
	int y = m_frame.m_y;
	if (m_pParent)
	{
		const Rect & parentRect = m_pParent->GetFrame();
		x += parentRect.m_x;
		y += parentRect.m_y;
	}

	// check old position
	pT->setColor(GetBackgroundColor());
	if (m_frame != m_oldFrame)
	{
		pT->fillRect(x, y, x + m_frame.m_dx, y + m_frame.m_dy);
		pT->drawRect(x - m_iBorderWidth, y - m_iBorderWidth, x + m_frame.m_dx + m_iBorderWidth, y + m_frame.m_dy + m_iBorderWidth);

		m_oldFrame = m_frame;

		// draw background
		pT->fillRect(x, y, x + m_frame.m_dx, y + m_frame.m_dy);
	}

	if (GetBorderWidth() > 0)
	{
		// draw border
		pT->setColor(GetBorderColor());
		pT->drawRect(x - m_iBorderWidth, y - m_iBorderWidth, x + m_frame.m_dx + m_iBorderWidth, y + m_frame.m_dy + m_iBorderWidth);
	}
	else
	{
		// clear border
		pT->drawRect(x - m_iBorderWidth, y - m_iBorderWidth, x + m_frame.m_dx + m_iBorderWidth, y + m_frame.m_dy + m_iBorderWidth);
	}

	// draw focus

	// draw childs
	vector<ViewClass *>:: iterator it = m_aSubViews.begin();
	for (; it != m_aSubViews.end(); it++)
	{
		(*it)->OnDraw(pDisplay);
	}

	SetChanges(false);
}
// View.h

#ifndef _VIEW_h
#define _VIEW_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include <vector>

using namespace std;
class DisplayClass;

struct Rect
{
	int m_x;
	int m_y;
	int m_dx;
	int m_dy;

	Rect()
	{
		m_x = m_y = m_dx = m_dy = 0;
	}

	Rect(int x, int y, int dx, int dy)
	{
		m_x = x;
		m_y = y;
		m_dx = dx;
		m_dy = dy;
	}

	Rect & operator = (const Rect & other)
	{
		m_x = other.m_x;
		m_y = other.m_y;
		m_dx = other.m_dx;
		m_dy = other.m_dy;

		return *this;
	}

	bool operator == (const Rect & rect)
	{
		return (m_x == rect.m_x && m_y == rect.m_y && m_dx == rect.m_dx && m_dy == rect.m_dy);
	}

	bool operator != (const Rect & rect)
	{
		return (m_x != rect.m_x || m_y != rect.m_y || m_dx != rect.m_dx || m_dy != rect.m_dy);
	}
};

class ViewClass
{
 protected:
	Rect m_frame;
	ViewClass * m_pParent;
	vector<ViewClass *> m_aSubViews;
	String m_sName;

	bool IsChanged();

private:
	Rect m_oldFrame;
	word m_wBackgroundColor;
	word m_wBorderColor;
	unsigned int m_iBorderWidth;
	bool m_bHasFocus;
	bool m_bHasChanges;
	void RemoveView(ViewClass * pView);

	void GetRealFrameRecursive(ViewClass *pParent, Rect* pRect);
	void SetChangesRecursive(ViewClass * pView, bool bChanges);

 public:
	ViewClass(ViewClass * pParent = 0);
	virtual ~ViewClass();

	void SetName(const String & sText);
	const String & GetName();
	const Rect & GetFrame();
	void GetRealFrame(Rect & frame);
	void SetFrame(const Rect & frame);
	void AddSubView(ViewClass * pView);
	const vector<ViewClass *> * GetSubViews();
	void RemoveFromSuperview();
	const word GetBackgroundColor();
	void SetBackgroundColor(word wBackgroundColor);
	const word GetBorderColor();
	void SetBorderColor(word wBorderColor);
	const unsigned int GetBorderWidth();
	void SetBorderWidth(unsigned int iBorderWidth);
	void SetFocus();
	void ReleaseFocus();
	ViewClass * const GetParent();

	void SetChanges(bool bChanges = true);

	virtual void OnDraw(DisplayClass * pDisplay);
};

#endif


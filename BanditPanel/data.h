// data.h

#ifndef _DATA_h
#define _DATA_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

typedef struct EScreenData
{
	int m_iDisplayWidth;
	int m_iDisplayHeight;
	uint8_t m_iXFontSize;
	uint8_t m_iYFontSize;
} ScreenData;

extern ScreenData g_ScreenData;

extern uint8_t SmallFont[];
extern uint8_t BigFont[];
extern uint8_t SevenSegNumFont[];

#endif

